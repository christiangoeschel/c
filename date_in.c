/* Name: date_in.c
 * Purpose: Enter date in format mm/dd/yy and print it in yymmdd
 * Author: Christian Goeschel Ndjomouo
 * Date written: 31 Jan 2024
 */

#include <stdio.h>

int main(){
	int d,m,y;
	printf("Please enter a date in format MM/DD/YY: ");
	scanf("%d/%d/%d",&m,&d,&y);
	printf("You entered the date %.2d%.2d%.2d\n", y,m,d);
	return 0;
}
