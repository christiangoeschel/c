/* Name: formattednums.c
 * Purpose: Format a float and integer with different conversion specifiers
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>

int main(){
	int i = 40;
	float x = 38.234;
	
	printf("|%%X|%%5X|%%-5X|%%5.3X|%%.3X|\n");
	printf("|%d|%5d|%-5d|%5.3d|%.3d|\n", i,i,i,i,i);
	printf("|%.f|%5f|%-5f|%12.5f|%.3f|\n", x,x,x,x,x);
	printf("|%8.1e|%-10.6e|%8.3f|%-6f", x,x,x,x);
	return 0;
}
