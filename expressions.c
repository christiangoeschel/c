// [ Name ]:            expressions.c
// [ Purpose ]:         Exercise on different types of expression usage
// [ Author ]:          Christian Goeschel Ndjomouo
// [ Date written ]:    6. February 2024

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXIT_SUCCESS 0

int main(){

    int input;

    // Exercise 3 & 4
    printf("[ Exercise 3 & 4 ]\n");
    printf("Please enter a three digit number: ");
    scanf("%3d", &input);
    printf("Entered number: %1d%1d\n", input % 10, input / 10);

    // Exercise 5
    int n1,n2,n3;
    printf("[ Exercise 5 ]\n");
    printf("Please enter a three digit number: ");
    scanf("%1d%1d%1d", &n1,&n2,&n3);
    printf("Entered number: %1d%1d%1d\n", n3,n2,n1);

    int i, j, k;
    // Exercise 6
    // a) Answer: 63, 8
    printf("[ Exercise 6 ]\n");
    i = 7;
    j = 8;
    i *= j + 1;
    printf("%d %d\n", i, j);

    // b) Answer: 3,2,1
    i = j = k = 1;
    i += j += k;
    printf("%d %d %d\n", i, j, k);

    // c) Answer: 2,-1,3
    i = 1;
    j = 2;
    k = 3;
    i -= j -= k;
    printf("%d %d %d\n", i, j, k);

    // d) Answer: 0,0,0
    i = 2;
    j = 1;
    k = 0;
    i *= j *= k;
    printf("%d %d %d\n", i, j, k);


    exit(EXIT_SUCCESS);
}




