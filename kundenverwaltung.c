#include <stdio.h>
#include <stdbool.h>

// Kundenlisten Arrays 
int kunden_IDs[] = {100,2,3};
char kunden_Namen[][49] = {"Chris","Jean","Jesse"};

// Produktlisten Arrays
int produkt_IDs[] = {1,2};
char produkt_Namen[][49] = {"Apfel","Banane"};


// Funktion zur Menuausgabe
int print_Menu(){
	
	int options_Auswahl;
	char menu_Optionen[5][49] = {"Neuen Kunden eintragen","Kunden ausgeben","Neues Produkt eintragen","Produkt ausgeben","Beenden"};
	
	printf("====        MENU        ====\n\n");
	printf("Bitte eine Auswahl eingeben\n");

	for(int i=0;i<5;i++){
		printf("%d ",i+1);
		printf("%s\n", menu_Optionen[i]);
	}

	printf("\nAuswahl: ");
	scanf("%d", &options_Auswahl);
	return options_Auswahl;			
}

// Funktion sucht nach der Kundeneingabe in dem definierten Array
bool ID_InArray(int array[], int id,int array_Index){
	
	bool isInArray = false;
		
	if(array[array_Index] == id){	
		
		isInArray = true;
	}

	return isInArray;
}

// Funktion die Arrays mapped
void array_Mapper(int array[],char mapped_Array[][49],int id){
	
	int array_Length = sizeof(*array);	
	
	for(int i=0;i<array_Length;i++){
        	if(ID_InArray(array,id,i) == true){
                		
			printf("\nKundennummer: %d",array[i]);
			printf("\nKundenname: %s\n", mapped_Array[i]);
			break;
            	}
		else{
			continue;
		}
       	}	
}



// Funtion fuer den Kontrollfluss
void auswahl_Auswertung(int a){
	
	int options_Auswahl = a;
	int id;

	
	switch(options_Auswahl){
		case 1:
			printf("\nDeine Auswahl lautet %d", options_Auswahl);
			break;
		case 2:
			printf("\nBitte Kundennummer eingeben: ");
			scanf("%d", &id);
			array_Mapper(kunden_IDs,kunden_Namen,id);
			break;
		case 3:
			printf("\nDeine Auswahl lautet %d", options_Auswahl);
			break;
		case 4:
			printf("\nBitte Produktnummer eingeben: ");
			scanf("%d", &id);
			array_Mapper(produkt_IDs,produkt_Namen,id);
			break;
		case 5:
			printf("\nProgramm wird beendet...");
			break;

		default:
			printf("Ungueltige Auswahl! Programm wird beendet ...");
			break;
	}

}


int main(){
		
	auswahl_Auswertung(print_Menu());
	return 0;
}
