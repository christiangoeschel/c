
// [Name]:		date_fmt_conv.c 
// [Description]:	Date format conversion 
// [Author]:		Christian Goeschel 
// [Date written]:	02 Mar 2024 

#include <stdio.h>
#include <string.h>

int main(){
	
	int d,m,y;
	char day_suffix[] = "th";
	char months[12][4] = { {'J','a','n','\0'},{'F','e','b','\0'},{'M','a','r','\0'},{'A','p','r','\0'},{'M','a','y','\0'},{'J','u','n','\0'},{'J','u','l','\0'},{'A','u','g','\0'},{'S','e','p','\0'},{'O','c','t','\0'},{'N','o','v','\0'},{'D','e','c','\0'} };	
	
	printf("Please type in a date in format MM/DD/YYYY:");
	scanf("%d/%d/%d", &d,&m,&y);

	switch (d){
		case 1: case 21: case 31: strcpy(day_suffix, "st"); break;
		case 2: case 22: strcpy(day_suffix, "nd"); break;
	}
			
	printf("Dated this %2.2d%s day of %s, %4.4d\n", d,day_suffix,months[m-1],y);

return 0;

}

