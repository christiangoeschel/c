
// [Name]:		switch_case.c 
// [Description]:	Switch cases 
// [Author]:	Christian Goeschel 
// [Date written]:	02 Mar 2024 
#include <stdio.h>


int main(){

int a = 5;
switch (a){
	case 5:		printf("Yes!\n");
			break;
	default: 	printf("No!\n");
			break;


}


return 0;

}
