/* Name: dweight.c
 * Purpose: Computes the dimensional weight of a 12" x 10" x 8" box
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>

int main(){
	int height = 8, length =  12, width = 10;
 	int volume = height * length * width;	
	
	printf("Dimensions: %dx%dx%d\n", length, width, height);
	printf("Volume (cubic inches): %d\n", volume);
	printf("Dimensional weight (pounds): %d\n", (volume + 165) / 166);
	return 0;
}
