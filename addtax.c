/* Name: addtax.c
 * Purpose: Prompt user to enter $ amount and return the amount with a 5% tax
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>

int main(){
	float user_input;
	printf("Please enter a dollar amount: ");
	scanf("%f", &user_input);
	printf("With tax added: %.2f\n", user_input + (user_input * 0.05));
	return 0;
}
