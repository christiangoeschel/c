/* Name: pun.c 
 * Purpose: Print a bad pun 
 * Author: Christian Goeschel Ndjomouo 
 * Date written: 29/01/2024 
 */

#include <stdio.h>

int main(){
	int age=24;	
	int your_age;	
	
	printf("To C, or not to C: that is the question.\n");
	printf("My age: %d\nWhat is your age:", age);
	scanf("%d", &your_age);
	printf("\nYour age: %d\n", your_age);
	return 0;
}
