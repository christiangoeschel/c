/* Name: isbn_brkdwn.c
 * Purpose: Enter an ISBN and print details about each code component
 * Author: Christian Goeschel Ndjomouo
 * Date written: 31 Jan 2024
 */

#include <stdio.h>

int main(){

	int lang, pub, book_num, check_digit;
	printf("Please enter a valid hyphen-seperated ISBN: ");
	scanf("%d-%d-%d-%d", &lang, &pub, &book_num, &check_digit);
	printf("Language: %d\nPublisher: %d\nBook number: %d\nCheck digit: %d\n", lang, pub, book_num, check_digit);
	return 0;

}
