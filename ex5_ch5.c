
// [Name]:		ex5_ch5.c 
// [Description]:	C Programming Modern approach - Chapter 5 Exercises 
// [Author]:	Christian Goeschel 
// [Date written]:	02 Mar 2024 
#include <stdio.h>
#include <string.h>

int main(){
	// Ex. 1
	// a)
	int i = 5;
	int j = 7; 
	int k = i * j == 34;
	printf("%d\n", k);	
	
	// b)
	k = 10;
	printf("%d\n", k < i > j);
	
	// c)
	printf("%d\n", i < j == k > j);
	
	// d)
	printf("%d\n", j % i + i < k);
	

	// Ex. 2
	// a)
	printf("%d\n", (!i) < k);
	// b)
	printf("%d\n", (!!i) + (!i));
	// c)
	k = 0;
	printf("%d\n", i && k || j);
	// d)
	printf("%d\n", i < j || k);

	// Ex. 3
	// a)
	k = 10;
	printf("%d\n", i < j && ++j < k);
	printf("%d\n", j);

	// Ex. 4
	i = 10;
	j = 10;
	printf("%d\n", i != j ? (i < j ? -1 : 1) : (i - i) );

	int h, m;
	char time_fmt[] = "AM";	
	if (h > 12){
		strcpy(time_fmt, "PM");
	}

	printf("Please type in a time: ");
	scanf("%d:%d", &h,&m);
	printf("%2.2d:%2.2d %s", (h > 12 ? h - 12 : h), m, time_fmt);

	return 0;
}
