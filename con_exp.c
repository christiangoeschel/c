
// [Name]:		con_exp.c 
// [Description]:  	Conditional expression
// [Author]:		Christian Goeschel
// [Date written]:	02 Mar 2024 

#include <stdio.h>
#include <string.h>


int main(int argc, const char *argv[]){

	int a = 5;
	int b = 6;
	int true_msg = 1;
	int false_msg = 0;
	printf("%s\n", a > b ? "True" : "False");			

return (a > b ? true_msg : false_msg);

}
