/* Name: floatsnints.c
 * Purpose: Declare floats and integers without initializer and print them
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>

int main(){
	int a, b, c, d, e;
	float f, g, h, i, j;
	printf("Integers: %d, %d, %d, %d, %d\n", a,b,c,d,e);
	printf("Floats: %f, %f, %f, %f, %f\n", f,g,h,i,j);
	return 0;
}
