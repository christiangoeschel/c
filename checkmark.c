/* Name: checkmark.c
 * Purpose: Print a checkmark made of "*"
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>

int main(){
	printf("       *\n      *\n     *\n*   *\n * *\n  *\n");
	return 0;
}
