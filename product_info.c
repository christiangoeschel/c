/* Name: product_info.c
 * Purpose: Enter three product attributes and display them in a structure table
 * Author: Christian Goeschel Ndjomouo
 * Date written: 31 Jan 2024
 */

#include <stdio.h>

int main(){
	int item_num, year, day, month;
	float item_price;	
	
	printf("Enter item number: ");
	scanf("%d", &item_num);
	printf("Enter unit price: ");
	scanf("%f", &item_price);
	printf("Enter purchase date (mm/dd/yy): ");
	scanf("%d/%d/%d", &month, &day, &year);
	
	printf("Item\t\tUnit\t\tPurchase\n\t\tPrice\t\tDate\n%d\t\t$ %.2f\t\t%.2d/%.2d/%.2d\n", item_num, item_price, month, day, year);
	
	return 0;
}
