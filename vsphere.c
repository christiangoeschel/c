/* Name: vsphere.c
 * Purpose: Compute the volume of a sphere
 * Author: Christian Goeschel Ndjomouo
 * Date written: 30 Jan 2024
 */

#include <stdio.h>
#include <math.h>
#define NUM_PI 3.14159265359

int main(){
	int r;
	float volume;
	printf("Please enter the Sphere's radius: ");
	scanf("%d", &r);  
	volume = ( 4 / 3. ) * NUM_PI * pow(r,3);
	printf("Sphere volume: %.2f\n", volume);
	return 0;
}
