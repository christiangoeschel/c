
// [Name]:		windforce.c 
// [Description]:	Wind force evaluator 
// [Author]:		Christian Goeschel 
// [Date written]:	03 Mar 2024 

// Code starts here ...
#include <stdio.h>
#include <stdlib.h>

int main(){
	
	float wind_speed;
	
	printf("Please enter the wind speed in knots: ");
	scanf("%f", &wind_speed);
	
	if( 1 > wind_speed && wind_speed > 0 )
		printf("Calm!\n");
	else if( 1 <= wind_speed && wind_speed <= 3)
		printf("Light Air\n");
	else if( 4 <= wind_speed && wind_speed <= 27 )
		printf("Breeze\n");
	else if( 28 <= wind_speed && wind_speed <= 47 )
		printf("Gale\n");	
	else if( 48 <= wind_speed && wind_speed <= 63 )
		printf("Storm\n");	
	else if( wind_speed > 63)
		printf("Hurricane!!!\n");
	else{ 
		printf("Invalid input!\n");
		exit(EXIT_FAILURE);		
	}

exit(EXIT_SUCCESS);
}
